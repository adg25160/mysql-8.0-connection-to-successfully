<%@ page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="BIG5">
<title>Insert title here</title>
</head>
<body>
<%
//Mysql 8.0 測試資料庫是否連接成功
//https://dev.mysql.com/downloads/connector/j/8.0.html 驅動下載網址
//選擇Platform Independent，下載ZIP解壓縮mysql-connector-java-8.0.15.jar到
//C:\Program Files\Apache Software Foundation\Tomcat 9.0\lib
//原com.mysql.jdbc.Driver加上cj
//新版連接方式需要在網址後面加上SSL與伺服器的時區
Connection conn = null;
try{
	Class.forName("com.mysql.cj.jdbc.Driver");
	conn =DriverManager.getConnection("jdbc:mysql://localhost:3306/tcm?useSSL=false&serverTimezone=UTC",
									  "root",       //帳號
									  "");          //密碼
	if(conn!=null){
		out.println("connection successfully to database");
	}
}catch(Exception e){
	out.println("not connection to database");
}
%>
</body>
</html>